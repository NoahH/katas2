function add(num1, num2){
    return num1 + num2;
}
function multiply(num1, num2){
    let retValue = 0;
    for(let i = 0; i < num1; i ++)
        retValue = add(retValue, num2);
    return retValue;
}
function power(base, power){
    let retValue = 1;
    for(let i = 0; i < power; i ++)
        retValue = multiply(retValue, base);
    return retValue;
}
function factorial(num){
    let retValue = 1;
    for(;num > 0; num --)
        retValue = multiply(retValue, num);
    return retValue;
}
function fibonacci(num){
    let arr = [0, 1];
    for(let i = 2; i < num; i ++)
        arr.push(add(arr[i-1], arr[i-2]));
    return arr[num - 1];
}